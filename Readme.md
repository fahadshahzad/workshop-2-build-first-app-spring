### Mysql Community Server/Workbench -> Application.properties configurations
```
spring.datasource.url=jdbc:mysql://localhost:3306/workshop1?useSSL=false&allowPublicKeyRetrieval=true
spring.datasource.username=root
spring.datasource.password=Fahad@123
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.hibernate.ddl-auto=update
logging.level.org.hibernate.sql=DEBUG
logging.level.org.hibernate.type=TRACE
server.port=9091
```
### Mysql PhpMyAdmin (Xampp Server) -> Application.properties configurations
```
spring.datasource.url=jdbc:mysql://localhost:3306/mms?useUnicode=true&useJDBCCompliantTimezoneShif=true&useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=create-drop
logging.level.org.hibernate.sql=DEBUG
logging.level.org.hibernate.type=TRACE
server.port=9090
```
### PostgreSQL -> Application.properties configurations
```
spring.datasource.url= jdbc:postgresql://localhost:5432/pos
spring.datasource.username=postgres
spring.datasource.password=123
spring.jpa.database-platform = org.hibernate.dialect.PostgreSQLDialect
spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults=false
spring.jpa.show-sql=true
spring.jpa.generate-ddl=false
spring.jpa.hibernate.ddl-auto=update
server.port=9091
spring.datasource.hikari.maximum-pool-size=500
spring.datasource.hikari.connection-timeout=50000000000
spring.datasource.hikari.leak-detection-threshold=60000
spring.datasource.hikari.minimum-idle=20000
spring.datasource.hikari.auto-commit=false
spring.datasource.hikari.idle-timeout=60000
spring.datasource.hikari.max-lifetime=100000
```
### H2 Apache Derby Database-> Application.properties configurations
```
spring.h2.console.enabled=true
```
### Dependencies-> Application.properties configurations
```
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.23</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/com.h2database/h2 -->
       <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
```
